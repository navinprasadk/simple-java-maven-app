pipeline {
    // agent { docker { image 'maven:3.3.3' } }
    agent any
    
    tools {
        jdk 'java'
        maven 'myMaven'
    }
    
    environment {
        NEXUS_URL = "http://52.140.32.56:8890/repository"
    }
      
    stages {
    
        stage('Build') {
            steps {
                echo "**********Building the source code**********"
                configFileProvider([configFile(fileId: "maven_global_settings", variable: 'SETTINGS')]) {
                    sh 'mvn -gs $SETTINGS -B -DskipTests clean package'
                    }
            }
        }
    
        stage('Unit test') {
            steps {
                echo "**********Unit testing the source code**********"
                configFileProvider([configFile(fileId: "maven_global_settings", variable: 'SETTINGS')]) {
                    sh 'mvn -gs $SETTINGS test jacoco:report'
                    }
            }
            post {
                always {
                    junit 'target/surefire-reports/*.xml'
                }
            }
        }
    
        stage('Code analysis') {
            steps {
                echo "**********Analysing the source code using sonarqube**********"
                withSonarQubeEnv(credentialsId: 'sonar-acess-token', installationName: 'SonarQube-Server') {
                    configFileProvider([configFile(fileId: "maven_global_settings", variable: 'SETTINGS')]) {
                            sh 'mvn -gs $SETTINGS package sonar:sonar'
                          }
                }
            }
        }
        
        stage('Quality gate') {
            steps {
                echo "**********checking the quality gate**********"
                // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                // true = set pipeline to UNSTABLE, false = don't
                waitForQualityGate abortPipeline: true
            }
        }
        stage('Artifact upload') {
            steps {
                echo "**********Uploading the artifacts to nexus**********"
                script {
                    def mavenPom = readMavenPom file: 'pom.xml'
                    def nexusRepoName = mavenPom.version.endsWith("SNAPSHOT") ? "maven-snapshots" : "maven-releases"
                    configFileProvider([configFile(fileId: "maven_global_settings", variable: 'SETTINGS')]) {
                        sh "mvn -gs $SETTINGS deploy:deploy-file -DgeneratePom=false -DrepositoryId=nexus -Durl=${NEXUS_URL}/${nexusRepoName} -DpomFile=pom.xml -Dfile=target/my-app-${mavenPom.version}.jar"
                        // echo "********version from pom.xml*******"
                        // echo "${mavenPom.version}"
            
                    }
                    
                }
            }
        }
    }
}
